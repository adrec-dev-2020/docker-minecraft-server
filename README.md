How to create a simple Minecraft Docker image
=============================================

## Building the image

- Open a terminal from the current directory (were the Dockerfile is located).
- Run the build command `docker build -t my-minecraft-server .`

You can add some options when building:
- `--progress=plain`: print all the operation to your terminal.
- `--no-cache`: disable cache when building your image.
- `-t YOUR_IMAGE_NAME`: give a name to your image
- `-t YOUR_IMAGE_NAME:tag`: give a name and a tag to your image.

## Start a new container

- Open a terminal (does not matter where)
- Run te run command ` docker run -p 25565:25565 -e EULA=1 my-minecraft-server`
- Wait for the server to up. (Can take time of first start, cause generating the world)

You can add some options:
- `-p port_host:port_container`: Use to link a port from your host machine to a port from the container. Here, Minecraft listing on 25565.
- `-e VAR=value`: Pass environment variable to the container. Here, set `EULA=1` to accept Minecraft eula.
- `-v VOL_NAME:/path/inside/container`: Mounting a volume to a specific path inside the container. Volume can shared files between container (and save data).
- `-v /path/from/host:/path/inside/container`: Second type of volume, mount a directory from host machine.

### More Info

Server data are save inside the container in path `/srv/minecraft/data`. Recommend to mount a volume on it.

### Examples of full command:
```shell script
# Windows
docker run -p 25565:25565 -e EULA=1 -v c:\minecraft:/srv/minecraft/data my-minecraft-server

# Linux
docker run -p 25565:25565 -e EULA=1 -v /tmp/minecraft:/srv/minecraft/data my-minecraft-server
```

## License

This code is release under MIT license.

## Useful links

- Official page to download server: https://www.minecraft.net/en-us/download/server
- Good tutorial to setup a server: https://minecraft.gamepedia.com/Tutorials/Setting_up_a_server
- Docker documentation: https://docs.docker.com/
