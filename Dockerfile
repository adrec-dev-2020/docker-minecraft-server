# First, we need to download the server.jar from official website.
# Using first stage (rename as dl-srv) to download it.
FROM debian as dl-srv

# Using one command line with && to declare with one RUN all apt + wget.
# => one cache docker build for all apt + wget.
RUN apt-get update && \
    apt-get install wget -y && \
    wget -O /tmp/server.jar https://launcher.mojang.com/v1/objects/1b557e7b033b583cd9f66746b7a9ab1ec1673ced/server.jar

# Stage 2, the real image.
# Minecraft need a java environment to run.
# Choose openjdk because its open source.
FROM openjdk:8

# Create an environment variable inside the image. This variable will be present in the future containers.
# This variable is used to accept Minecraft's EULA from docker run.
ENV EULA=0

# Create a new directory to put all Minecraft configuration and data files.
RUN mkdir -p /srv/minecraft

# Copy download file server.jar from the stage 1.
COPY --from=dl-srv /tmp/server.jar /srv/minecraft/server.jar
# Copy the file entrypoint.sh from the building machine.
COPY entrypoint.sh /srv/minecraft/entrypoint.sh

# Change working directory.
WORKDIR /srv/minecraft

# Add execution perms to entrypoint.sh
RUN chmod a+x entrypoint.sh

# Expose Minecraft gaming port.
EXPOSE 25565

# On container start. Exec entrypoint.sh as main program.
ENTRYPOINT ["/srv/minecraft/entrypoint.sh"]
