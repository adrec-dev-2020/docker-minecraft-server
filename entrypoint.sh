#!/bin/bash

# Check if the directory exist.
if [[ ! -d /srv/minecraft/data ]]; then
  # If not, create it.
  mkdir -p /srv/minecraft/data
fi

# Move to the previous checked directory.
cd /srv/minecraft/data

# Check if the server.jar exist inside current working directory (/srv/minecraft/data)
if [[ ! -f server.jar ]]; then
  # If not, copy it.
  cp /srv/minecraft/server.jar server.jar
fi

# Check if eula.txt exist.
if [[ -f eula.txt ]]; then
  echo "EULA exist";
else
  # If not, check if the EULA variable exist.
  if [[ ! -v EULA ]]; then
    # If not print info and exit with error code.
    echo "EULA ENV does not exist."
    exit 1
  fi

  # If exist, then check that is value is equal to 0.
  if [[ $EULA -eq 0 ]]; then
    # If true, mean that the EULA are not accepted => exit with error code.
    echo "You have to accept EULA."
    exit 2
  fi

  # If all ok, create the eula.txt file with accepted value inside.
  echo "eula=true" > eula.txt
fi

# Start the server.
java -Xmx1024M -Xms1024M -jar server.jar nogui
